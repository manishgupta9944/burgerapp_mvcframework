﻿using BurgerApp_MVCFramework.Models;
using BurgerApp_MVCFramework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BurgerApp_MVCFramework.Controllers
{
    public class BurgerController : Controller
    {
        BurgerService burgerServiceObject;
        readonly IBurgerService _burgerService;

        public BurgerController()
        {
            burgerServiceObject = new BurgerService();
            _burgerService = (IBurgerService)burgerServiceObject;
        }
        public ActionResult Index()
        {
            List<Burger> burgers = _burgerService.GetAllBurgers();
            return View(burgers);
        }

        [HttpGet]
        public ActionResult AddBurger()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddBurger(Burger burger)
        {
            bool burgerAddStatus = _burgerService.AddBurger(burger);
            return RedirectToAction("Index", "Burger");
        }


    }
}