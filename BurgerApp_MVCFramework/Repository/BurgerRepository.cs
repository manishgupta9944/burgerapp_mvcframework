﻿using BurgerApp_MVCFramework.Models;
using System;
using System.Collections.Generic;

namespace BurgerApp_MVCFramework.Repository
{
    public class BurgerRepository : IBurgerrepository
    {
        public bool AddBurger(Burger burger)
        {
            MvcApplication.burgerList.Add(burger);
            return true;
        }

        public List<Burger> GetAllBurgers()
        {
            return MvcApplication.burgerList;
        }

     
    }
}