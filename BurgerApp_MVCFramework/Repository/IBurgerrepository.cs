﻿using BurgerApp_MVCFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BurgerApp_MVCFramework.Repository
{
    public interface IBurgerrepository
    {
        List<Burger> GetAllBurgers();
        bool AddBurger(Burger burger);
    }
}
