﻿using BurgerApp_MVCFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BurgerApp_MVCFramework
{
    public class MvcApplication : System.Web.HttpApplication
    {
        internal static List<Burger> burgerList = new List<Burger>();
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            burgerList.Add(new Burger() { Id = 1, Name = "Burger-1", Price = 140 });
            burgerList.Add(new Burger() { Id = 2, Name = "Burger-2", Price = 141, IsVeg = false });
            burgerList.Add(new Burger() { Id = 3, Name = "Burger-3", Price = 142 });
            burgerList.Add(new Burger() { Id = 4, Name = "Burger-4", Price = 144, IsVeg = false });
            burgerList.Add(new Burger() { Id = 5, Name = "Burger-5", Price = 145 });

        }
    }
}
