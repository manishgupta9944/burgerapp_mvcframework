﻿using BurgerApp_MVCFramework.Models;
using System.Collections.Generic;
using BurgerApp_MVCFramework.Repository;
namespace BurgerApp_MVCFramework.Services
{
    public class BurgerService:IBurgerService
    {   
         IBurgerrepository _burgerRepository;
         BurgerRepository burgerRepository;

        public BurgerService()
        {   
            burgerRepository = new BurgerRepository();
            _burgerRepository = (IBurgerrepository)burgerRepository;
        }

        public bool AddBurger(Burger burger)
        {
            return _burgerRepository.AddBurger(burger);
        }

        public  List<Burger> GetAllBurgers()
        {
            return _burgerRepository.GetAllBurgers();
            
        }
    }
}