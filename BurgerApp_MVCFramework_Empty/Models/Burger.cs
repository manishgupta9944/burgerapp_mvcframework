﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BurgerApp_MVCFramework_Empty.Models
{
    public class Burger
    {   
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }

        public bool IsVeg { get; set; } = true;
    }
}