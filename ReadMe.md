# Project1 - ASPNET.MVCFramework.Empty
# Proejct2 - ASPNET.MVCFramework.MVC

# MCV - Model View Controller
	* Model---> Burger.cs
	* View----> cshtml
		* Normal View (Empty Without Model)
		  Nothing is added
		* Strongly Typed View (Get intellicance for adding properties) [Empty Model]
		  @model namespace.Models.className
		* Normal View () [Empty Without Model]
		  @model.dynamic
	* Controller->BurgerController.cs
	     * Index

	* Repository
	    IBurgerRepository.cs---> Interface
		BurgerRepository.cs ---> Class

	* Service
	    IBurgerService.cs ---> Interface
		BurgerService.cs --->  Class


Controller ---> Service ---> Repository

Controller ---> Serice --> Repository ---> ContextDB

![image-1](/images/image1.png)
![image-2](/images/image2.png)